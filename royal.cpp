#include <iostream>
#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include "roy++.hpp"

std::pair<std::string, std::string> parse_address(std::string const& addr)
{
  std::string::size_type const colon = addr.find(':');

  if (colon == std::string::npos) {
    return std::make_pair(addr, "9999");
  } else {
    return std::make_pair(addr.substr(0, colon), addr.substr(colon + 1));
  }
}

void loop(
    std::string const& host
  , std::string const& port
  , std::string const& tag
  , std::string const& unit
  , std::string const& desc
  , std::istream& input
)
{
  tridas::monitoring::MonStreamer ms(host, boost::lexical_cast<int>(port));
  tridas::monitoring::SimpleObservable obs(tag, unit, desc);

  while (input && input.peek() != EOF) {
    std::string line;
    std::getline(input, line);

    try {
      double const value = boost::lexical_cast<double>(line);

      ms << obs.put(value);
    } catch (boost::bad_lexical_cast const& e) {
    }
  }
}

int main(int argc, char* argv[])
{
  boost::program_options::options_description options("Options");
  try {
    std::string desc, tag, unit, address = "localhost:9999";

    options.add_options()
        ("help,h", "Print this help and exit.")
        (
          "address,a", boost::program_options::value<std::string>(&address)
            ->default_value(address)
            ->required()
            ->value_name("address"),
          "ROyWeb remote address and port."
        )
        (
          "tag,t", boost::program_options::value<std::string>(&tag)
            ->required()
            ->value_name("tag"),
          "Name of the observable."
        )
        (
          "unit,u", boost::program_options::value<std::string>(&unit)
            ->required()
            ->value_name("unit"),
          "Unit of the observable."
        )
        (
          "desc,d", boost::program_options::value<std::string>(&desc)
            ->required()
            ->value_name("description"),
          "Description of the observable."
        );

    boost::program_options::variables_map vm;

    boost::program_options::store(
        boost::program_options::command_line_parser(
            argc
          , argv
        ).options(options).run()
      , vm
    );

      if (vm.count("help")) {
      std::cout << "Usage: " << argv[0] << ' ' << options << '\n';
      return EXIT_SUCCESS;
    }

    vm.notify();

    std::pair<std::string, std::string> const remote = parse_address(
        address
    );

    loop(remote.first, remote.second, tag, unit, desc, std::cin);
  } catch (boost::program_options::error const& e) {
    std::cerr << "ROyal: Error: " << e.what() << '\n' << options;
    return EXIT_FAILURE;
  } catch (std::exception const& e) {
    std::cerr << "ROyal: Error: " << e.what() << '\n';
    return EXIT_FAILURE;
  } catch (...) {
    std::cerr << "Unknown error\n";
    return EXIT_FAILURE;
  }
}
